  require("src.board")
  require("src.player")
  require("src.tile")
  require("src.tools")
  require("src.imageAssets")
  require("src.sound")
  
board = {}
player = {}

levelNumber = 1
lives = 3

fontBig = {}
fontMedium = {}
fontSmall = {}

menu = {}

function love.load()
  if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
  fontSmall = love.graphics.newFont( "fonts//Pixel-Noir.ttf", 18 )
  fontMedium= love.graphics.newFont( "fonts//Pixel-Noir.ttf", 34 )
  fontBig= love.graphics.newFont( "fonts//Pixel-Noir.ttf", 40 )
  
  loadSounds()
  
  loadImages()
  
  --loadGame()
  loadMenu()
  
end

function loadMenu()
  board.status="menu"
  menu.i = 1
end

function loadGame()
  if(levelNumber>levelC) then
    board.status="win"
  else
  --board. initialization
  board = giveMeBoard(levelNumber)
  
  --player. initialization
  player = giveMePlayer()
  end
end

function love.draw()
  if(board.status=="win") then
    love.graphics.setBackgroundColor(0,0,0,255)
    love.graphics.setColor(255,0,0,255)
    love.graphics.setFont(fontBig)
    love.graphics.print("YOU HAVE WON!", love.graphics:getWidth()*0.25, love.graphics:getHeight()*0.2)
    
    love.graphics.setColor(255,255,255,255)
    love.graphics.draw(getImage("winpic"), love.graphics:getWidth()*0.38, love.graphics:getHeight()*0.4)
    
    love.graphics.setColor(255,0,0,255)
    love.graphics.setFont(fontMedium)
    love.graphics.print("so please, dont be so \"corpo\" anymore...", love.graphics:getWidth()*0.06, love.graphics:getHeight()*0.7)
    
    love.graphics.setColor(255,255,255,255)
    love.graphics.setFont(fontSmall)
    love.graphics.print("(and press esc to go to menu)", love.graphics:getWidth()*0.31, love.graphics:getHeight()*0.9)
    
  elseif(board.status=="lost") then
    love.graphics.setBackgroundColor(0,0,0,255)
    love.graphics.setColor(255,0,0,255)
    love.graphics.setFont(fontBig)
    love.graphics.print("YOU HAVE LOST!", love.graphics:getWidth()*0.25, love.graphics:getHeight()*0.2)
    
    love.graphics.setColor(255,255,255,255)
    love.graphics.draw(getImage("lostpic"), love.graphics:getWidth()*0.38, love.graphics:getHeight()*0.3)
    
    love.graphics.setColor(255,0,0,255)
    love.graphics.setFont(fontMedium)
    love.graphics.print("please die", love.graphics:getWidth()*0.38, love.graphics:getHeight()*0.7)
    
    love.graphics.setColor(255,255,255,255)
    love.graphics.setFont(fontSmall)
    love.graphics.print("(or press esc to go to menu)", love.graphics:getWidth()*0.31, love.graphics:getHeight()*0.9)
    --love.graphics.rectangle("fill", 0, 0, player.w, player.h)
  elseif(board.status=="menu") then
    love.graphics.setColor(255,255,255,255)
    love.graphics.draw(getImage("menu"), (love.graphics:getWidth()-700)/2, 0)
    
    love.graphics.setColor(255,0,0,255)
    --love.graphics.rectangle("fill", 330, 210, 280, 60)
    --love.graphics.rectangle("fill", 330, 310, 200, 60)
  else
    love.graphics.setBackgroundColor(255,255,255,255)
     --draw board.
    drawBoard(board,player)
      
     --draw player
    
    drawGUI(love.graphics:getWidth()*0.8, love.graphics:getHeight()*0.1)
  end
end

function drawGUI(x,y)
    local enter=50
    love.graphics.setFont(fontSmall)
    love.graphics.setColor(255,255,255,255)
    
    love.graphics.print("Lives: "..lives, x,y)
    love.graphics.print("Level: "..levelNumber, x,y+1*enter)
    love.graphics.print("Fps: "..love.timer.getFPS(), x,y+2*enter)
end


function love.keypressed(key)
  if(board.status=="menu")then
    if key == "return" or key==" " then
      lives = 3
      levelNumber = 1
      loadGame()
    end
    
    if key == "escape" then
      love.event.quit()
   end
  else

   if key == "1" then
     levelNumber = 1
     loadGame()
   end
   if key == "2" then
     levelNumber = 2
     loadGame()
   end
   if key == "3" then
     levelNumber = 3
     loadGame()
   end
  if key == "0" then
     levelNumber = 0
     loadGame()
   end
   if key == "escape" then
      loadMenu()
   end
  end
     
end

function love.mousepressed(x, y, button)
  if(board.status=="menu") then
   if button == "l" then
      if(x>330) and (x<330+280) and (y>210) and (y<210+60) then
        lives = 3
        levelNumber = 1
        loadGame()
      elseif(x>330) and (x<330+200) and (y>310) and (y<310+60) then
        love.event.quit()
      end
   end
  end
end

function love.update(dt)
  
  if(board.status == "game") then
    -- keyboard. actions for player
    
    -- check for move in vertical and horizontal axis
    local move = {}
    move.v=0
    move.h=0
    
    updateBombs(board, dt)
    updateEnemys(board, dt)
    updateExplosions(board, dt)
    updatePlayer(player,board,dt)
    
    local moved = false
      
    if love.keyboard.isDown(" ") then
      putBomb(board, getField(player))
    end


    if love.keyboard.isDown("left") then  
      --movePlayerLeft(player, dt)
      moved = true
      move.h = -1
    elseif love.keyboard.isDown("right") then
      --movePlayerRight(player, dt)
      moved = true
      move.h = 1
    end
    if love.keyboard.isDown("up") then
      --movePlayerUp(player, dt)
      moved = true
      move.v = -1
    elseif love.keyboard.isDown("down") then
      --movePlayerDown(player,dt)
      moved = true
      move.v = 1
    end
    
    if (moved) then
      -- analyze if move is legal due to colysion with board tiles
      local hipoteticalX = giveHipoteticalX(player, dt, move.h)
      local rectH = getRectangle(player)
      rectH.x = hipoteticalX
      if (not checkColisionWithBoard(board, rectH) ) then
        movePlayerHorizontaly(player, dt, move.h)
      end
        
      
      local hipoteticalY = giveHipoteticalY(player, dt, move.v)
      local rectV = getRectangle(player)
      rectV.y = hipoteticalY
      if (not checkColisionWithBoard(board, rectV) ) then
        movePlayerVerticaly(player, dt, move.v)
      end
    end
    updateStatus(board,player)
  elseif(board.status=="dead") then
   lives = lives - 1
   if(lives>0) then
      loadGame()
   else
      board.status="lost"
   end
  elseif(board.status=="levelup") then
    levelNumber=levelNumber+1
    loadGame()
  elseif(board.status=="win") then
  
  elseif(board.status=="lost") then
  end

end


