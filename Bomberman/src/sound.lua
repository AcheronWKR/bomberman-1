sounds = {}


function loadSounds()
  sounds["bombPlacement"] = love.audio.newSource("sounds//bomb_placement.wav", "static")
  sounds["explosion1"] = love.audio.newSource("sounds//explosion1.wav", "static")
  sounds["explosion2"] = love.audio.newSource("sounds//explosion2.wav", "static")
  sounds["explosion3"] = love.audio.newSource("sounds//explosion3.wav", "static")
  
  sounds["footstep1"] = love.audio.newSource("sounds//footstep1.wav", "static")
  sounds["footstep2"] = love.audio.newSource("sounds//footstep2.wav", "static")
  sounds["footstep3"] = love.audio.newSource("sounds//footstep3.wav", "static")
  sounds["footstep4"] = love.audio.newSource("sounds//footstep4.wav", "static")
  sounds["footstep5"] = love.audio.newSource("sounds//footstep2.wav", "static")
  
  sounds["footstep1"]:setVolume(0.6)
  sounds["footstep2"]:setVolume(0.6)
  sounds["footstep3"]:setVolume(0.6)
  sounds["footstep4"]:setVolume(0.6)
  sounds["footstep5"]:setVolume(0.6)
  
  sounds["music"] = love.audio.newSource("sounds//music.mp3", "static")
  sounds["music"]:setLooping(true)
  sounds["music"]:setVolume(0.3)
  sounds["music"]:play()
end

function playSound(sound)
  sounds[sound]:play()
end

function playExplosion()
  local rand = math.random(1,3)
  sounds["explosion"..rand]:play()
end