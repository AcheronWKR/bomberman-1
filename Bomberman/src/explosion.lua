require("src.imageAssets")
--require("src.board")

Explosion = {}
Explosion.x = 0
Explosion.y = 0 
Explosion.w = 0
Explosion.h = 0
Explosion.timer = 0
Explosion.positionX=0
Explosion.positionY=0

Explosion.blocking = false
Explosion.destroyable = false

Explosion.texture = getImage("Explosion")


function Explosion:new ()
  o = {}   -- create object if user does not provide one
  setmetatable(o, self)
  self.__index = self
  return o
end

function Explosion:newExplosion (board,i,j)
  o = Explosion:new()
  o.x = board.x + i * board.tileSize
  o.y = board.y + j * board.tileSize
  
  o.w = board.tileSize
  o.h = board.tileSize
  
  o.texture = getImage("explosionanim")
  o.anim = newAnimation(getImage("explosionanim"), 50, 50, 0.2, 0)
  o.timer = 0
  return o
end

function Explosion:isBlocking()
  return self.blocking
end

function Explosion:setBlocking()
  self.blocking = true
end

function Explosion:isDestroyable()
  return self.destroyable
end
  
function Explosion:drawMe()
  if( self.anim) then
    self.anim:draw(self.x,self.y)
    --love.graphics.draw(self.texture, self.x,self.y)
  end
end

function Explosion:updateExplosion(board, dt)
  self.anim:update(dt)
  self.timer=self.timer+dt
  if(self.timer>board.explosionTime) then
    board.explosions={}
    return false
  end
  return true
end



