require("src.imageAssets")
require("src.tools")
require("lib.anal")

function giveMePlayer()
  local player = {}
  player.x=board.x+board.tileSize
  player.y=board.y+board.tileSize
  player.Size=board.w/board.sizeX
  player.w=player.Size
  player.h=player.Size
  player.color1 = {}
  player.color1[0] = 128
  player.color1[1] = 255
  player.color1[2] = 128
  player.color1[3] = 255
  player.color2 = {}
  player.color2[0] = 0
  player.color2[1] = 0
  player.color2[2] = 0
  player.color2[3] = 255
  player.speed = 100
  player.bomblimit = 100
  player.texture = getImage("player")
  player.animd = newAnimation(getImage("playerd"), 50, 50, 0.4, 0)
  player.animu = newAnimation(getImage("playeru"), 50, 50, 0.4, 0)
  player.animl = newAnimation(getImage("playerl"), 50, 50, 0.4, 0)
  player.animr = newAnimation(getImage("playerr"), 50, 50, 0.4, 0)
  player.animCurr = player.animd
  player.deadTexture = getImage("playerdead")
  player.deadTimer = 0
  player.deadTime = 1.5
  player.status = "live"
  player.stepSound = "footstep3"
  player.stepTime = 1
  player.stepTimer = 0
  return player
end

function drawPlayer(player)
    --love.graphics.setColor(player.color2[0],player.color2[1],player.color2[2],player.color2[3])
    --love.graphics.circle("fill",player.x+player.w/2,player.y+player.h/2,player.Size/2)
    
    --love.graphics.setColor(player.color1[0],player.color1[1],player.color1[2],player.color1[3])
    --love.graphics.circle("fill",player.x+player.w/2,player.y+player.h/2,(player.Size*0.8)/2)
    --love.graphics.rectangle("fill", player.x, player.y, player.w, player.h)
    
    --love.graphics.draw(player.texture, player.x, player.y)
    if(player.status=="live") then
      player.animCurr:draw(player.x,player.y)
    elseif(player.status=="dying") then
      love.graphics.draw(player.deadTexture, player.x, player.y)
    end
      
end

function updatePlayer(player,board,dt)
  if(player.status=="dying") then
    player.deadTimer=player.deadTimer+dt
    if(player.deadTimer>player.deadTime) then
      player.status="dead"
    end
  end
end

function movePlayerLeft(player, dt)
  if(board.x<player.x) then
    player.x = player.x - player.speed*dt
  end
end

function movePlayerRight(player, dt)
  if( (board.x + board.w) > (player.x + player.w) ) then
    player.x = player.x + player.speed*dt
  end
end

function movePlayerUp(player, dt)
  if(board.y<player.y) then
    player.y = player.y - player.speed*dt
  end
end

function movePlayerDown(player, dt)
  if( (board.y + board.h) > (player.y + player.h) ) then
    player.y = player.y + player.speed*dt
  end
end

function giveHipoteticalX(player, dt, h)
  return player.x + player.speed*dt*h
end
  
function giveHipoteticalY(player, dt, v)
  return player.y + player.speed*dt * v
end
  
  
function movePlayerVerticaly(player, dt, v)
  if(player.status~="live")then
    return
  end
  player.y = player.y + player.speed*dt * v
  if(v>0) then
    player.animCurr = player.animd
  elseif(v<0) then
    player.animCurr = player.animu
  end
  stepsUpdate(player, dt)
end

function movePlayerHorizontaly(player, dt, h)
  if(player.status~="live")then
    return
  end
  player.x = player.x + player.speed*dt * h
  if(h>0) then
    player.animCurr = player.animr
  elseif(h<0) then
    player.animCurr = player.animl
  end
  stepsUpdate(player, dt)
end

function stepsUpdate(player, dt)
  if(player.stepTimer==0) then
    playSound(player.stepSound)
  else
    player.stepTimer= player.stepTimer+dt
  end
  
  if(player.stepTimer>player.stepTime) then
    player.stepTimer=0
  end
  player.animCurr:update(dt)
end

function getField(player)
  local rect = {}
  rect.x = board.x + board.tileSize * math.round( (player.x - board.x) / board.tileSize)
  rect.y = board.y + board.tileSize * math.round( (player.y - board.y) / board.tileSize)
  rect.w = board.tileSize
  rect.h = board.tileSize
  return rect
end 
  
  