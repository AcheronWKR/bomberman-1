require("src.imageAssets")
require("src.tools")
--require("src.board")

Enemy = {}
Enemy.kind = ""
Enemy.x = 0
Enemy.y = 0 
Enemy.w = 0
Enemy.h = 0
Enemy.direction = "l"
Enemy.speed = 1
Enemy.positionX=0
Enemy.positionY=0


Enemy.status="live"
Enemy.deadTimer=0
Enemy.deadTime=1.5
Enemy.blocking = false
Enemy.destroyable = false
Enemy.updateAnim = true

Enemy.texture = getImage("Enemy")


function Enemy:new ()
  o = {}   -- create object if user does not provide one
  setmetatable(o, self)
  self.__index = self
  return o
end

function Enemy:newEnemy (nkind, nX, nY , nW, nH)
  o = Enemy:new()
  
  o.status="live"
  o.kind = nkind
  o.x = nX
  o.y = nY
  
  o.w = nW
  o.h = nH
  
  o.deadTimer=0
  o.deadTime=1
  o.texture = getImage(nkind)
  o.deadTexture = getImage(nkind.."dead")
  o.animd = newAnimation(getImage(nkind.."d"), 50, 50, 0.4, 0)
  o.animu = newAnimation(getImage(nkind.."u"), 50, 50, 0.4, 0)
  o.animl = newAnimation(getImage(nkind.."l"), 50, 50, 0.4, 0)
  o.animr = newAnimation(getImage(nkind.."r"), 50, 50, 0.4, 0)
  o.animCurr = o.animd
  o.speed = 70  
  if(nkind=="1") then
    o.speed = 70
  elseif(nkind=="2") then
    o.speed = 0
    o.animCurr = o.animr
    o.updateAnim = false
  elseif(nkind=="3") then
    o.speed = 85
  elseif(nkind=="4") then
    o.speed = 120
  elseif(nkind=="5") then
    o.speed = 0
    o.animCurr = o.animl
    o.updateAnim = false
  end
  
  o.stepSound = "footstep"..nkind
  o.stepTime = 1
  o.stepTimer = 0
  
  local rand = math.random(1,4)
  if(rand==1) then
    o.direction="u"
  elseif(rand==2) then
    o.direction="d"
   elseif(rand==3) then
    o.direction="l"
  elseif(rand==4) then
    o.direction="r"
  end
  
  
  return o
end

function Enemy:isBlocking()
  return self.blocking
end

function Enemy:setBlocking()
  self.blocking = true
end

function Enemy:isDestroyable()
  return self.destroyable
end
  
function Enemy:drawMe()
  if( self.animCurr) then
    if(self.status=="live") then
      --love.graphics.draw(self.texture, self.x,self.y)
      self.animCurr:draw(self.x,self.y)
    elseif(self.status=="dying") then
      love.graphics.draw(self.deadTexture, self.x,self.y)
    end
  end
end

function Enemy:findIndex(table, tableMax)
  for i=1, tableMax+1, 1 do
    if(table[i]==self) then
      return i
    end
  end
  return -1
end

function Enemy:updateEnemy(board, dt)
  if(self.updateAnim) then
    self.animCurr:update(dt)
  end
  if(self.status=="live") then
    
    if(self.stepTimer==0) then
      playSound(self.stepSound)
    else
      self.stepTimer= self.stepTimer+dt
    end
    
    if(self.stepTimer>self.stepTime) then
      self.stepTimer=0
    end
    
    if(self.kind=="1" or self.kind=="2"  or self.kind=="5" ) then
      self:move1(board,dt)
    elseif(self.kind=="3")then
      self:move3(board,dt)
    elseif(self.kind=="4")then
      self:move4(board,dt)
    end
  
    if(checkColisionWithExplosions(board, self)) then
      self.status="dying"
    end
  end
  
  if(self.status=="dying") then
    self.deadTimer=self.deadTimer+dt
    if(self.deadTimer>self.deadTime) then
      self.status="dead"
    end
  end
  
  
  if(self.status=="dead") then
      --table.remove(board.enemys, board.enemysC)
      if(board.enemysC<=1) then
        board.enemys={}
      else
          local temp = self:findIndex(board.enemys, board.enemysC)
          if(temp == board.enemysC) then
            board.enemys[temp]={}
          else
            board.enemys[temp] = board.enemys[board.enemysC]
            board.enemys[board.enemysC] = {}
          end
      end
      board.enemysC= board.enemysC-1
  end
end

function Enemy:move(nx,ny)
    if(nx>self.x) then
      self.animCurr=self.animr
    end
    
    if(nx<self.x) then
      self.animCurr=self.animl
    end
    
    if(ny>self.y) then
      self.animCurr=self.animd
    end
    
    if(ny<self.y) then
      self.animCurr=self.animu
    end
  
  self.x = nx
  self.y = ny
 end


function Enemy:move1(board, dt)
  local newX = self.x + dt*self.speed*getHforDirection(self.direction)
  local newY = self.y + dt*self.speed*getVforDirection(self.direction)
  
  local tempRect = getRectangle(self)
  tempRect.x = newX
  tempRect.y = newY
  
  if (checkColisionWithBoard(board, tempRect) ) then
    local rand = math.random(4)
    if(rand == 1) then
      self.direction = "u"
    elseif(rand==2) then
      self.direction = "d"
    elseif(rand==3) then
      self.direction = "l"
    else
      self.direction = "r"
    end
  else
    self:move(newX, newY)
  end
end

function Enemy:move3(board, dt)
  local tmpDirection = self.direction
  local i=1
  local possibleDirections = {}
  local directions = {}
  local directionChange = false
 
  table.insert(directions,1,"u")
  table.insert(directions,2,"d")
  table.insert(directions,3,"l")
  table.insert(directions,4,"r")


  local tmpRect = 0
  local tmpX = 0
  local tmpY = 0
  for i=1, #directions, 1 do
    local tmpDir1 = directions[i]
    local tmpDir2 = opositeDirection(self.direction)
    local tmpDir3 = self.direction
    if(directions[i]~=opositeDirection(self.direction)) then
      tmpRect = getRectangle(self)
      tmpRect.x = self.x + dt*self.speed*getHforDirection(directions[i])
      tmpRect.y = self.y + dt*self.speed*getVforDirection(directions[i])
        if (not (checkColisionWithBoard(board, tmpRect)) ) then
          --possibleDirections:insert(directions[i])
          table.insert(possibleDirections, directions[i])
        end
    end
  end
  
  if(#possibleDirections>1) then
    --possibleDirections:add(self.directions)
    local rand = math.random(1,#possibleDirections)
    directionChange = true
    self.direction = possibleDirections[rand]

  end
  
  
  local newX = self.x + dt*self.speed*getHforDirection(self.direction)
  local newY = self.y + dt*self.speed*getVforDirection(self.direction)
  
  local tempRect = getRectangle(self)
  tempRect.x = newX
  tempRect.y = newY
  
  if (checkColisionWithBoard(board, tempRect) ) then
    if(not directionChange) then
      local rand = math.random(4)
      if(rand == 1) then
        self.direction = "u"
      elseif(rand==2) then
        self.direction = "d"
      elseif(rand==3) then
        self.direction = "l"
      else
        self.direction = "r"
      end
    end
  else
    self:move(newX, newY)
  end
  
  if(tmpDirection==opositeDirection(self.direction) ) then
    local a=0
  end
end

function Enemy:move4(board, dt)
  local tmpDirection = self.direction
  local i=1
  local possibleDirections = {}
  local directions = {}
  local directionChange = false
 
  local tmpDir=""
  local tmpRect = 0
  local tmpX = 0
  local tmpY = 0
  if (detectVerticalCollision(self,player)) then
    if((self.x-player.x)>=0) then
      tmpDir="l"
    else
      tmpDir="r"
    end
  end
  
  if(detectHorizontalCollision(self,player)) then
    if((self.y-player.y)>=0) then
      tmpDir="u"
    else
      tmpDir="d"
    end
  end
  
  tmpRect = getRectangle(self)
  tmpX = self.x + dt*self.speed*getHforDirection(tmpDir)
  tmpY = self.y + dt*self.speed*getVforDirection(tmpDir)
  tmpRect.x=tmpX
  tmpRect.y=tmpY
  
  
  if(tmpDir~="") then
    if(not checkColisionWithBoard(board, tmpRect)) then
      self.direction=tmpDir
      directionChange = true
    end
  end 
  
  local newX = self.x + dt*self.speed*getHforDirection(self.direction)
  local newY = self.y + dt*self.speed*getVforDirection(self.direction)
  
  local tempRect = getRectangle(self)
  tempRect.x = newX
  tempRect.y = newY
  
  
  if (checkColisionWithBoard(board, tempRect) ) then
    if(not directionChange) then
      local rand = math.random(4)
      if(rand == 1) then
        self.direction = "u"
      elseif(rand==2) then
        self.direction = "d"
      elseif(rand==3) then
        self.direction = "l"
      else
        self.direction = "r"
      end
    end
  else
    self:move(newX, newY)
  end
  
  if(tmpDirection==opositeDirection(self.direction) ) then
    local a=0
  end
end