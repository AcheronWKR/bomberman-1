function getRectangle(object)
  local rectangle = {}
  rectangle.x = object.x
  rectangle.y = object.y
  rectangle.w = object.w
  rectangle.h = object.h
  
  return rectangle
end

precision = 0.5

function math.round(n, deci) deci = 10^(deci or 0) return math.floor(n*deci+.5)/deci end

function detectHorizontalCollision(o1, o2)
  local r1={}
  r1.left  = o1.x+precision
  r1.right = o1.x + o1.w-precision
  
  local r2={}
  r2.left  = o2.x+precision
  r2.right = o2.x + o2.w - precision
  
  if ( ( r1.left <= r2.left) and (r2.left <= r1.right) ) then
   return true
  end
  
  if ( (r1.left <= r2.right) and (r2.right <= r1.right) ) then
    return true
  end
  
  return false
end

function detectVerticalCollision(o1, o2)
  local r1={}
  r1.top = o1.y + precision
  r1.bot = o1.y + o1.h - precision
  
  local r2={}
  r2.top = o2.y + precision
  r2.bot = o2.y + o2.h - precision

  if ( (r1.top <= r2.top) and (r2.top <= r1.bot) ) then
    return true
  end
    
  if ( (r1.top <= r2.bot) and (r2.bot <= r1.bot) ) then
    return true
  end
  
  return false
end

function detectCollision(o1, o2)
  if (o1 == nil) then
    local nilError = 1
  end
  
  if (o2 == nil) then
    local nilError = 1
  end
  
  if ( detectHorizontalCollision(o1,o2) and detectVerticalCollision(o1,o2) ) then
    return true
  else
    return false
  end
end

function getVforDirection(direction)
  local result=0
  if(direction=="u") then
    result = -1
  end
  
  if (direction=="d") then
    result = 1
  end
  return result
end

function getHforDirection(direction)
  local result=0
  if(direction=="l") then
    result = -1
  end
  
  if (direction=="r") then
    result = 1
  end
  return result
end

 function getHitbox (object)
  if(object.hitbox == nil or object.hitbox.x==nil or object.hitbox.y==nil or object.hitbox.w==nil or object.hitbox.h==nil) then
    return nil
  end
  local debug = getRectangle(object)
  local rect = {}
  rect.x=object.x+object.hitbox.x*object.w
  rect.y=object.y+object.hitbox.y*object.h
  rect.w=object.w-object.hitbox.w*object.w-object.hitbox.x*object.w
  rect.h=object.h-object.hitbox.h*object.h-object.hitbox.y*object.h
  
  local fuckme=0
  if (debug.x~=rect.x) then
    fuckme=1
  end
  
  if (debug.y~=rect.y) then
    fuckme=1
  end
  
  if (debug.w~=rect.w) then
    fuckme=1
  end
  
  if (debug.h~=rect.h) then
    fuckme=1
  end
  
  return rect
  
 end    
    
function opositeDirection(direction)
  if(direction=="u") then
    return "d"
  end
  
  if(direction=="d") then
    return "u"    
  end
  
  if(direction=="l") then
    return "r"  
  end
  
  if(direction=="r") then
    return "l"
  end
  
  return nil
end