require("src.imageAssets")
--require("src.board")

Bomb = {}
Bomb.x = 0
Bomb.y = 0 
Bomb.w = 0
Bomb.h = 0
Bomb.timer = 0
Bomb.positionX=0
Bomb.positionY=0

Bomb.blocking = false
Bomb.destroyable = false

Bomb.texture = getImage("bomb")


function Bomb:new ()
  o = {}   -- create object if user does not provide one
  setmetatable(o, self)
  self.__index = self
  return o
end

function Bomb:newBomb (rectangle)
  local o = Bomb:new()
  o.x = rectangle.x
  o.y = rectangle.y
  
  o.w = rectangle.w
  o.h = rectangle.h
  
  o.hitbox={}
  o.hitbox.x=0.4
  o.hitbox.y=0.3
  o.hitbox.w=0
  o.hitbox.h=0
  
  
  o.positionX = (o.x-board.x)/board.tileSize
  o.positionY = (o.y-board.y)/board.tileSize
  
  o.texture = getImage("bomb")
  o.timer = 0
  return o
end

function Bomb:isBlocking()
  return self.blocking
end

function Bomb:setBlocking()
  self.blocking = true
end

function Bomb:isDestroyable()
  return self.destroyable
end
  
function Bomb:drawMe()
  if( self.texture) then
    love.graphics.draw(self.texture, self.x,self.y)
  end
end

function Bomb:updateBomb(board, dt)
  self.timer=self.timer+dt
  if(self.timer>board.bombTime) then
    bombExplode()
  end
end



