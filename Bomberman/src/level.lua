levelC = 3

level1 = {};

level1[0] =  {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}
level1[1] =  {"Z","Y","A","B","F","G","K","Y","G","Y","2","B","Z"}
level1[2] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","M","Z"}
level1[3] =  {"Z","Y","Y","Y","Y","Y","Y","Y","F","Y","2","B","Z"}
level1[4] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","M","Z"}
level1[5] =  {"Z","Y","Y","Y","Y","Y","Y","Y","A","Y","2","B","Z"}
level1[6] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","M","Z"}
level1[7] =  {"Z","Y","Y","Y","Y","T","5","Y","G","Y","2","B","Z"}
level1[8] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","M","Z"}
level1[9] =  {"Z","Y","Y","Y","1","Y","Y","A","F","Y","2","B","Z"}
level1[10] = {"Z","Y","X","A","X","Y","X","Y","X","M","X","M","Z"}
level1[11] = {"Z","Y","A","Y","A","Y","Y","Y","Y","Y","Y","F","Z"}
level1[12] = {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}

level1.x = 11
level1.y = 11

level2 = {};

level2[0] =  {"X","X","X","X","X","X","X","X","X","X","X","X","X"}
level2[1] =  {"X","Y","Y","Y","Z","Y","Y","Y","Y","Y","Y","Y","X"}
level2[2] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[3] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[4] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[5] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[6] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[7] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[8] =  {"X","Z","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[9] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[10] = {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[11] = {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[12] = {"X","X","X","X","X","X","X","X","X","X","X","X","X"}

level2.x = 3
level2.y = 3

level3 = {};

level3[0] =  {"X","X","X","X","X","X","X","X","X","X","X","X","X"}
level3[1] =  {"X","Y","Y","Y","Z","Y","Y","Y","Y","Y","Y","Y","X"}
level3[2] =  {"X","Y","X","Z","X","Y","X","Y","X","Y","X","Y","X"}
level3[3] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[4] =  {"X","Y","X","Z","X","Y","X","Y","X","Y","X","Y","X"}
level3[5] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[6] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level3[7] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[8] =  {"X","Y","X","Y","X","4","X","Y","X","Y","X","Y","X"}
level3[9] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","3","Y","Y","X"}
level3[10] = {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level3[11] = {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[12] = {"X","X","X","X","X","X","X","X","X","X","X","X","X"}

level3.x = 1
level3.y = 4

level0={}
level0[0] =  {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}
level0[1] =  {"Z","Y","A","B","F","G","K","S","Y","Y","Y","Y","Z"}
level0[2] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","Y","Z"}
level0[3] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[4] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","Y","Z"}
level0[5] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[6] =  {"Z","Y","X","Y","X","Y","X","Y","X","Y","X","Y","Z"}
level0[7] =  {"Z","Y","Y","Y","Y","T","2","Y","Y","S","Y","Y","Z"}
level0[8] =  {"Z","Y","X","Y","X","Y","X","Y","X","A","X","Y","Z"}
level0[9] =  {"Z","Y","Y","Y","1","Y","Y","A","Y","4","A","Y","Z"}
level0[10] = {"Z","Y","X","A","X","Y","X","Y","X","A","X","Y","Z"}
level0[11] = {"Z","Y","A","3","A","Y","Y","Y","Y","Y","Y","M","Z"}
level0[12] = {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}

level0.x = 5
level0.y = 5

function getLevel(n)
  if (n==nil) then
    return level1
  end
  
  if (n==1) then
    return level1
  end
    
  if (n==2) then
    return level2
  end
    
  if (n==3) then
    return level3
  end
  
  if (n==0) then
    return level0
  end

end 

  